/*  This file (modulelist.h) is part of Doom Module Launcher - A Doom engine frontend.
 *
 *  Copyright (C) Omnisthetics
 *
 *  Doom Module Launcher is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Doom Module Launcher is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Doom Module Launcher.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MODULELIST_H_
#define MODULELIST_H_

#include "./module.h"
#include "./loadlist.h"
#include <QTreeView>
#include <QMouseEvent>
#include <QDragMoveEvent>
#include <QStandardItemModel>

class ModuleList : public QTreeView
{
private:
	QStandardItemModel * loadModel;
public:
	ModuleList();
	ModuleList(QStandardItemModel * newLoadModel);
	void operator=(const ModuleList & other);
	void mouseDoubleClickEvent(QMouseEvent * event);
};

#endif
