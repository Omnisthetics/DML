/*  This file (mainview.cxx) is part of Doom Module Launcher - A Doom engine frontend.
 *
 *  Copyright (C) Omnisthetics
 *
 *  Doom Module Launcher is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Doom Module Launcher is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Doom Module Launcher.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "./mainview.h"
#include <QFileDialog>
#include <cstdlib>
#include <iostream>

MainView::MainView() : QObject()
{
}

MainView::MainView(std::vector<IWAD> * newIwads, std::vector<Module> * newModules,
		std::string newPath) : QObject()
{
	//Initialise pointers
	iwads = newIwads;
	modules = newModules;
	iwadModel = new QSortFilterProxyModel();
	moduleModel = new QSortFilterProxyModel();
	loadModel = new QStandardItemModel();

	//Load icons from local folder
	plusIcon.addFile("./assets/plus.png");
	minusIcon.addFile("./assets/minus.png");
	dotsIcon.addFile("./assets/dots.png");
	playIcon.addFile("./assets/play.png");
	crossIcon.addFile("./assets/cross.png");
	tickIcon.addFile("./assets/tick.png");
	folderIcon.addFile("./assets/folder.png");
	upIcon.addFile("./assets/up.png");
	downIcon.addFile("./assets/down.png");
	infoIcon.addFile("./assets/info.png");

	//Load icons from home directory folder
	QString assetFolder = QString(getenv("HOME")) + QString("/.local/share/dml/assets/");
	plusIcon.addFile(assetFolder + QString("plus.png"));
	minusIcon.addFile(assetFolder + QString("minus.png"));
	dotsIcon.addFile(assetFolder + QString("dots.png"));
	playIcon.addFile(assetFolder + QString("play.png"));
	crossIcon.addFile(assetFolder + QString("cross.png"));
	tickIcon.addFile(assetFolder + QString("tick.png"));
	folderIcon.addFile(assetFolder + QString("folder.png"));
	upIcon.addFile(assetFolder + QString("up.png"));
	downIcon.addFile(assetFolder + QString("down.png"));
	infoIcon.addFile(assetFolder + QString("info.png"));

	//Set up play button
	playButton = new QPushButton();
	playButton->setIcon(playIcon);
	playButton->setToolTip("Play!");
	playButton->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
	QObject::connect(playButton, &QPushButton::clicked, this, &MainView::execute);

	//Set up info bar
	infoTextLabel = new QLabel();
	infoTextLabel->setText("Doom Module Launcher v1.0");

	infoIconLabel = new QLabel();
	QSize infoIconSize(infoTextLabel->fontMetrics().height(),
			infoTextLabel->fontMetrics().height());
	infoIconLabel->setMaximumSize(infoIconSize);
	infoIconLabel->setPixmap(infoIcon.pixmap(infoIconSize));

	//Set up engine path section
	enginePathLabel = new QLabel();
	enginePathField = new QLineEdit();
	engineBrowseButton = new QPushButton();
	enginePathLabel->setText("Engine Executable Path and Parameters");
	enginePathField->setText(newPath.c_str());
	engineBrowseButton->setIcon(folderIcon);
	engineBrowseButton->setToolTip("Browse Engine Executable");
	engineBrowseButton->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
	QObject::connect(engineBrowseButton, &QPushButton::clicked, this, &MainView::browseEngine);

	//Set up layout
	iwadLayout = new QVBoxLayout();
	moduleLayout = new QVBoxLayout();
	loadLayout = new QVBoxLayout();
	initIWADList();
	initModuleList();
	initLoadList();

	infoLayout = new QHBoxLayout();
	infoLayout->addWidget(infoIconLabel);
	infoLayout->addWidget(infoTextLabel);

	layout = new QGridLayout();
	layout->addLayout(iwadLayout, 0, 0, 1, 2);
	layout->addLayout(moduleLayout, 0, 2, 1, 2);
	layout->addLayout(loadLayout, 0, 4, 1, 2);
	layout->addWidget(enginePathLabel, 1, 0, 1, 4);
	layout->addWidget(enginePathField, 2, 0, 1, 4);
	layout->addWidget(engineBrowseButton, 1, 4, 2, 1);
	layout->addWidget(playButton, 1, 5, 2, 1);
	layout->addLayout(infoLayout, 3, 0, 1, -1);
}

MainView::~MainView()
{
	//Delete layout
	delete layout;
}

//=================================================================================================
// Menu Initialisers
//=================================================================================================

/* Initialises the contents of the IWAD list menu and adds it to the IWAD layout.
 */
void MainView::initIWADList()
{
	//Initialise dynamic memory pointers
	iwadLabel = new QLabel();
	iwadList = new QListView();
	iwadButtonLayout = new QHBoxLayout();
	addIWADButton = new QPushButton();
	editIWADButton = new QPushButton();
	removeIWADButton = new QPushButton();

	//Set proper menu label text
	iwadLabel->setText("IWADs");

	//Initialise IWAD list
	iwadList->setModel(iwadModel);
	iwadList->setEditTriggers(QAbstractItemView::NoEditTriggers);
	updateIWADModel();

	//Initialise button arrangement
	iwadButtonLayout->addWidget(addIWADButton);
	iwadButtonLayout->addWidget(editIWADButton);
	iwadButtonLayout->addWidget(removeIWADButton);

	//Initialise IWAD menu layout
	iwadLayout->addWidget(iwadLabel);
	iwadLayout->addWidget(iwadList);
	iwadLayout->addLayout(iwadButtonLayout);

	//Initialise buttons
	addIWADButton->setIcon(plusIcon);
	addIWADButton->setToolTip("Add");
	editIWADButton->setIcon(dotsIcon);
	editIWADButton->setToolTip("Edit");
	removeIWADButton->setIcon(minusIcon);
	removeIWADButton->setToolTip("Remove");
	QObject::connect(addIWADButton, &QPushButton::clicked, this, &MainView::openAddIWAD);
	QObject::connect(editIWADButton, &QPushButton::clicked, this, &MainView::openEditIWAD);
	QObject::connect(removeIWADButton, &QPushButton::clicked, this, &MainView::removeIWAD);
}

/* Initialises the contents of the add IWAD menu and adds it to the IWAD layout.
 */
void MainView::initAddIWAD()
{
	//Initialise pointers
	editIWADLayout = new QVBoxLayout();
	editIWADFormLayout = new QFormLayout();
	editIWADButtonLayout = new QHBoxLayout();
	editIWADMenuLabel = new QLabel();
	editIWADNameLabel = new QLabel();
	editIWADNameField = new QLineEdit();
	editIWADFilepathLabel = new QLabel();
	editIWADFilepathField = new QLineEdit();
	editIWADBrowseButton = new QPushButton();
	editIWADConfirmButton = new QPushButton();
	editIWADCancelButton = new QPushButton();

	//Initialise form labels
	editIWADMenuLabel->setText("Add IWAD");
	editIWADNameLabel->setText("Name:");
	editIWADFilepathLabel->setText("Filepath:");

	//Initialise buttons
	editIWADBrowseButton->setIcon(folderIcon);
	editIWADBrowseButton->setToolTip("Browse");
	editIWADConfirmButton->setIcon(tickIcon);
	editIWADConfirmButton->setToolTip("Confirm");
	editIWADCancelButton->setIcon(crossIcon);
	editIWADCancelButton->setToolTip("Cancel");
	QObject::connect(editIWADBrowseButton, &QPushButton::clicked, this, &MainView::browseIWAD);
	QObject::connect(editIWADConfirmButton, &QPushButton::clicked, this, &MainView::addIWAD);
	QObject::connect(editIWADCancelButton, &QPushButton::clicked, this, &MainView::openIWADList);

	//Initialise form layout
	editIWADFormLayout->addRow(editIWADMenuLabel);
	editIWADFormLayout->addRow(editIWADNameLabel, editIWADNameField);
	editIWADFormLayout->addRow(editIWADFilepathLabel, editIWADFilepathField);
	editIWADLayout->addLayout(editIWADFormLayout);
	editIWADButtonLayout->addWidget(editIWADBrowseButton);
	editIWADButtonLayout->addWidget(editIWADConfirmButton);
	editIWADButtonLayout->addWidget(editIWADCancelButton);
	editIWADLayout->addLayout(editIWADButtonLayout);
	iwadLayout->addLayout(editIWADLayout);
}

/* Initialises the contents of the edit IWAD menu and adds it to the IWAD layout. Contents of
 * the currently selected IWAD are added into the appropriate fields.
 */
void MainView::initEditIWAD(std::string name)
{
	//Initialise pointers
	editIWADLayout = new QVBoxLayout();
	editIWADFormLayout = new QFormLayout();
	editIWADButtonLayout = new QHBoxLayout();
	editIWADMenuLabel = new QLabel();
	editIWADNameLabel = new QLabel();
	editIWADNameField = new QLineEdit();
	editIWADFilepathLabel = new QLabel();
	editIWADFilepathField = new QLineEdit();
	editIWADBrowseButton = new QPushButton();
	editIWADConfirmButton = new QPushButton();
	editIWADCancelButton = new QPushButton();

	//Initialise form labels
	editIWADMenuLabel->setText("Edit IWAD");
	editIWADNameLabel->setText("Name:");
	editIWADFilepathLabel->setText("Filepath:");

	//Initialise buttons
	editIWADBrowseButton->setIcon(folderIcon);
	editIWADBrowseButton->setToolTip("Browse");
	editIWADConfirmButton->setIcon(tickIcon);
	editIWADConfirmButton->setToolTip("Confirm");
	editIWADCancelButton->setIcon(crossIcon);
	editIWADCancelButton->setToolTip("Cancel");
	QObject::connect(editIWADBrowseButton, &QPushButton::clicked, this, &MainView::browseIWAD);
	QObject::connect(editIWADConfirmButton, &QPushButton::clicked, this, &MainView::editIWAD);
	QObject::connect(editIWADCancelButton, &QPushButton::clicked, this, &MainView::openIWADList);

	//Initialise form layout
	editIWADFormLayout->addRow(editIWADMenuLabel);
	editIWADFormLayout->addRow(editIWADNameLabel, editIWADNameField);
	editIWADFormLayout->addRow(editIWADFilepathLabel, editIWADFilepathField);
	editIWADLayout->addLayout(editIWADFormLayout);
	editIWADButtonLayout->addWidget(editIWADBrowseButton);
	editIWADButtonLayout->addWidget(editIWADConfirmButton);
	editIWADButtonLayout->addWidget(editIWADCancelButton);
	editIWADLayout->addLayout(editIWADButtonLayout);
	iwadLayout->addLayout(editIWADLayout);

	//Fill fields with selected IWAD info
	editedIWAD = getIWAD(name);
	if (editedIWAD != NULL)
	{
		editIWADNameField->setText(editedIWAD->getName().c_str());
		editIWADFilepathField->setText(editedIWAD->getFilepath().c_str());
	}
}

/* Initialises the contents of the module list menu and adds it to the module layout.
 */
void MainView::initModuleList()
{
	//Initialise pointers
	moduleButtonLayout = new QHBoxLayout();
	moduleLabel = new QLabel();
	moduleList = new ModuleList(loadModel);
	addModuleButton = new QPushButton();
	editModuleButton = new QPushButton();
	removeModuleButton = new QPushButton();

	//Initialise label
	moduleLabel->setText("Modules");

	//Set up module list
	moduleList->setModel(moduleModel);
	moduleList->setHeaderHidden(true);
	updateModuleModel();

	//Initialise buttons
	addModuleButton->setIcon(plusIcon);
	addModuleButton->setToolTip("Add");
	editModuleButton->setIcon(dotsIcon);
	editModuleButton->setToolTip("Edit");
	removeModuleButton->setIcon(minusIcon);
	removeModuleButton->setToolTip("Remove");
	QObject::connect(addModuleButton, &QPushButton::clicked, this, &MainView::openAddModule);
	QObject::connect(editModuleButton, &QPushButton::clicked, this, &MainView::openEditModule);
	QObject::connect(removeModuleButton, &QPushButton::clicked, this, &MainView::removeModule);

	//Initialise layout
	moduleLayout->addWidget(moduleLabel);
	moduleLayout->addWidget(moduleList);
	moduleButtonLayout->addWidget(addModuleButton);
	moduleButtonLayout->addWidget(editModuleButton);
	moduleButtonLayout->addWidget(removeModuleButton);
	moduleLayout->addLayout(moduleButtonLayout);
}

/* Initialises the contents of the add module menu and adds it to the module layout.
 */
void MainView::initAddModule()
{
	//Initialise pointers
	editModuleLayout = new QVBoxLayout();
	editModuleFormLayout = new QFormLayout();
	editModuleButtonLayout = new QHBoxLayout();
	editModuleMenuLabel = new QLabel();
	editModuleNameLabel = new QLabel();
	editModuleCategoryLabel = new QLabel();
	editModuleNameField = new QLineEdit();
	editModuleCategoryField = new QLineEdit();
	editModuleFileList = new QListView();
	editModuleBrowseButton = new QPushButton();
	editModuleRemoveButton = new QPushButton();
	editModuleConfirmButton = new QPushButton();
	editModuleCancelButton = new QPushButton();

	//Initialise form labels
	editModuleMenuLabel->setText("Add Module");
	editModuleNameLabel->setText("Name:");
	editModuleCategoryLabel->setText("Category:");

	//Initialise filelist
	fileModel = new QStandardItemModel();
	editModuleFileList->setModel(fileModel);
	editModuleFileList->setDragEnabled(true);
	editModuleFileList->setAcceptDrops(true);
	editModuleFileList->setDropIndicatorShown(true);
	editModuleFileList->setDragDropMode(QAbstractItemView::InternalMove);
	editModuleFileList->setDragDropOverwriteMode(false);
	editModuleFileList->setDefaultDropAction(Qt::MoveAction);
	editModuleFileList->setEditTriggers(QAbstractItemView::NoEditTriggers);

	//Initialise buttons
	editModuleBrowseButton->setIcon(folderIcon);
	editModuleBrowseButton->setToolTip("Browse");
	editModuleRemoveButton->setIcon(minusIcon);
	editModuleRemoveButton->setToolTip("Remove");
	editModuleConfirmButton->setIcon(tickIcon);
	editModuleConfirmButton->setToolTip("Confirm");
	editModuleCancelButton->setIcon(crossIcon);
	editModuleCancelButton->setToolTip("Cancel");
	QObject::connect(editModuleBrowseButton, &QPushButton::clicked, this,
			&MainView::browseModule);
	QObject::connect(editModuleRemoveButton, &QPushButton::clicked, this,
			&MainView::removeFileFromList);
	QObject::connect(editModuleConfirmButton, &QPushButton::clicked, this,
			&MainView::addModule);
	QObject::connect(editModuleCancelButton, &QPushButton::clicked, this,
			&MainView::openModuleList);

	//Initialise layouts
	editModuleFormLayout->addRow(editModuleMenuLabel);
	editModuleFormLayout->addRow(editModuleNameLabel, editModuleNameField);
	editModuleFormLayout->addRow(editModuleCategoryLabel, editModuleCategoryField);
	editModuleButtonLayout->addWidget(editModuleBrowseButton);
	editModuleButtonLayout->addWidget(editModuleRemoveButton);
	editModuleButtonLayout->addWidget(editModuleConfirmButton);
	editModuleButtonLayout->addWidget(editModuleCancelButton);
	editModuleLayout->addLayout(editModuleFormLayout);
	editModuleLayout->addWidget(editModuleFileList);
	editModuleLayout->addLayout(editModuleButtonLayout);
	moduleLayout->addLayout(editModuleLayout);
}

/* Initialises the contents of the edit module menu and adds it to the module layout. Contents of
 * the currently selected module are added into the appropriate fields.
 */
void MainView::initEditModule(std::string name)
{
	editedModule = getModule(name);
	if (editedModule != NULL)
	{
		//Initialise pointers
		editModuleLayout = new QVBoxLayout();
		editModuleFormLayout = new QFormLayout();
		editModuleButtonLayout = new QHBoxLayout();
		editModuleMenuLabel = new QLabel();
		editModuleNameLabel = new QLabel();
		editModuleCategoryLabel = new QLabel();
		editModuleNameField = new QLineEdit();
		editModuleCategoryField = new QLineEdit();
		editModuleFileList = new QListView();
		editModuleBrowseButton = new QPushButton();
		editModuleRemoveButton = new QPushButton();
		editModuleConfirmButton = new QPushButton();
		editModuleCancelButton = new QPushButton();

		//Initialise form labels
		editModuleMenuLabel->setText("Edit Module");
		editModuleNameLabel->setText("Name:");
		editModuleCategoryLabel->setText("Category:");

		//Initialise filelist
		fileModel = new QStandardItemModel();
		editModuleFileList->setModel(fileModel);
		editModuleFileList->setDragEnabled(true);
		editModuleFileList->setAcceptDrops(true);
		editModuleFileList->setDropIndicatorShown(true);
		editModuleFileList->setDragDropMode(QAbstractItemView::InternalMove);
		editModuleFileList->setDragDropOverwriteMode(false);
		editModuleFileList->setDefaultDropAction(Qt::MoveAction);
		editModuleFileList->setEditTriggers(QAbstractItemView::NoEditTriggers);

		//Initialise buttons
		editModuleBrowseButton->setIcon(folderIcon);
		editModuleBrowseButton->setToolTip("Browse");
		editModuleRemoveButton->setIcon(minusIcon);
		editModuleRemoveButton->setToolTip("Remove");
		editModuleConfirmButton->setIcon(tickIcon);
		editModuleConfirmButton->setToolTip("Confirm");
		editModuleCancelButton->setIcon(crossIcon);
		editModuleCancelButton->setToolTip("Cancel");
		QObject::connect(editModuleBrowseButton, &QPushButton::clicked, this,
				&MainView::browseModule);
		QObject::connect(editModuleRemoveButton, &QPushButton::clicked, this,
				&MainView::removeFileFromList);
		QObject::connect(editModuleConfirmButton, &QPushButton::clicked, this,
				&MainView::editModule);
		QObject::connect(editModuleCancelButton, &QPushButton::clicked, this,
				&MainView::openModuleList);

		//Initialise layouts
		editModuleFormLayout->addRow(editModuleMenuLabel);
		editModuleFormLayout->addRow(editModuleNameLabel, editModuleNameField);
		editModuleFormLayout->addRow(editModuleCategoryLabel, editModuleCategoryField);
		editModuleButtonLayout->addWidget(editModuleBrowseButton);
		editModuleButtonLayout->addWidget(editModuleRemoveButton);
		editModuleButtonLayout->addWidget(editModuleConfirmButton);
		editModuleButtonLayout->addWidget(editModuleCancelButton);
		editModuleLayout->addLayout(editModuleFormLayout);
		editModuleLayout->addWidget(editModuleFileList);
		editModuleLayout->addLayout(editModuleButtonLayout);
		moduleLayout->addLayout(editModuleLayout);

		//Fill fields with selected module info
		editModuleNameField->setText(editedModule->getName().c_str());
		editModuleCategoryField->setText(editedModule->getCategory().c_str());

		for (int i = 0; i < editedModule->getMods().size(); i++)
		{
			QStandardItem * item = new QStandardItem(editedModule->getMods()[i].c_str());
			item->setDropEnabled(false);
			fileModel->appendRow(item);
		}
	}
	else
	{
		infoTextLabel->setText("Module you wish to edit does not exist!");
		openModuleList();
	}
}

/* Initialises the contents of the load list menu and adds it to the load list layout.
 */
void MainView::initLoadList()
{
	//Initilise pointers
	loadButtonLayout = new QHBoxLayout();
	loadLabel = new QLabel();
	loadList = new LoadList(loadModel);
	moveLoadUpButton = new QPushButton();
	moveLoadDownButton = new QPushButton();
	removeLoadButton = new QPushButton();

	//Initialise label
	loadLabel->setText("Load list");

	//Adjust load list settings
	loadList->setModel(loadModel);
	loadList->setDragEnabled(true);
	loadList->setAcceptDrops(true);
	loadList->setDropIndicatorShown(true);
	loadList->setDragDropMode(QAbstractItemView::InternalMove);
	loadList->setDragDropOverwriteMode(false);
	loadList->setDefaultDropAction(Qt::MoveAction);

	//Initialise buttons
	moveLoadUpButton->setIcon(upIcon);
	moveLoadUpButton->setToolTip("Up");
	moveLoadDownButton->setIcon(downIcon);
	moveLoadDownButton->setToolTip("Down");
	removeLoadButton->setIcon(minusIcon);
	removeLoadButton->setToolTip("Remove");
	QObject::connect(moveLoadUpButton, &QPushButton::clicked, this, &MainView::moveLoadUp);
	QObject::connect(moveLoadDownButton, &QPushButton::clicked, this, &MainView::moveLoadDown);
	QObject::connect(removeLoadButton, &QPushButton::clicked, this, &MainView::removeLoad);

	//Initialise layout
	loadLayout->addWidget(loadLabel);
	loadLayout->addWidget(loadList);
	loadButtonLayout->addWidget(moveLoadUpButton);
	loadButtonLayout->addWidget(moveLoadDownButton);
	loadButtonLayout->addWidget(removeLoadButton);
	loadLayout->addLayout(loadButtonLayout);
}

//=================================================================================================
// Miscellaneous
//=================================================================================================

/* Generates a command string containing the selected engine executable, IWAD and load list module
 * files. The command is passed to the OS and executed.
 */
void MainView::execute()
{
	if (!enginePathField->text().toStdString().empty())
	{
		//Get IWAD selection list
		QModelIndexList selectionList = iwadList->selectionModel()->selectedIndexes();

		//Continue if an IWAD has been selected
		if (selectionList.isEmpty() == false)
		{
			//Get selected IWAD filepath
			std::string selectedIwadName = selectionList[0].data().toString().toStdString();
			std::string selectedIwadFilepath = getIWAD(selectedIwadName)->getFilepath();

			//Add module filenames to command string
			std::string pwads = " -file ";
			for (int i = 0; i < loadModel->rowCount(); i++)
			{
				//Get module
				std::string moduleName = loadModel->index(i, 0).data().toString().toStdString();
				Module * module = getModule(moduleName);

				//Get module filelist
				std::vector<std::string> mods = module->getMods();

				//Add individual file to pwad string
				for (int j = 0; j < mods.size(); j++)
				{
					pwads += '"' + mods[j] + '"' + ' ';
				}
			}

			//Execute command
			std::string command = enginePathField->text().toStdString() + " -iwad " + selectedIwadFilepath;
			if (loadModel->rowCount() > 0)
				command += pwads;
			system(command.c_str());
		}
		else
		{
			infoTextLabel->setText("An IWAD must be selected before launching!");
		}
	}
	else
	{
		infoTextLabel->setText("An executable is required to launch Doom!");
	}
}

/* Iterates through the IWAD vector and appropriately categorises its contents into a standard
 * item model. The model is then sorted in alphabetical order.
 */
void MainView::updateIWADModel()
{
	if (iwads != NULL)
	{
		QStandardItemModel * model = new QStandardItemModel();

		for (int i = 0; i < iwads->size(); i++)
		{
			//Create module entry
			QString nameStr = iwads->at(i).getName().c_str();
			QStandardItem * item = new QStandardItem(nameStr);
			model->appendRow(item);
		}

		//Create sorted proxy model
		iwadModel->setSourceModel(model);
		iwadModel->sort(0);
	}
	else
	{
		std::cerr << "No associated module vector in module list!" << std::endl;
	}
}

/* Iterates through the module vector and appropriately categorises its contents into a standard
 * item tree model. The tree model is then sorted in alphabetical order.
 */
void MainView::updateModuleModel()
{
	if (modules != NULL)
	{
		QStandardItemModel * model = new QStandardItemModel();
		QStandardItem * parentItem = model->invisibleRootItem();

		for (int i = 0; i < modules->size(); i++)
		{
			//Find category entry if it exists
			QString categoryStr = modules->at(i).getCategory().c_str();
			QList<QStandardItem *> categoryItem = model->findItems(categoryStr);

			if (categoryItem.size() == 0)
			{
				//Create category entry
				QStandardItem * category = new QStandardItem(categoryStr);
				parentItem->appendRow(category);

				//Create module entry
				QString nameStr = modules->at(i).getName().c_str();
				QStandardItem * item = new QStandardItem(nameStr);
				category->appendRow(item);
			}
			else
			{
				//Create module entry
				QString nameStr = modules->at(i).getName().c_str();
				QStandardItem * item = new QStandardItem(nameStr);
				categoryItem[0]->appendRow(item);
			}
		}

		//Create sorted proxy model
		moduleModel->setSourceModel(model);
		moduleModel->sort(0);
	}
	else
	{
		std::cerr << "No associated module vector in module list!" << std::endl;
	}
}

/* Clears the entirety of the desired layout by deleting the pointers themselves
 */
void MainView::clearLayout(QLayout * layout)
{
	QLayoutItem * item;
	while (item = layout->takeAt(0))
	{
		if (item->layout())
		{
			clearLayout(item->layout());
			delete item->layout();
		}
		else if (item->widget())
		{
			delete item->widget();
		}
	}
}

//=================================================================================================
// Menu Openers
//=================================================================================================

/* Replaces the add/edit IWAD menu with the IWAD list.
 */
void MainView::openIWADList()
{
	clearLayout(iwadLayout);
	initIWADList();
}

/* Replaces the add/edit module menu with the module list.
 */
void MainView::openModuleList()
{
	clearLayout(moduleLayout);
	initModuleList();
}

/* Replaces the IWAD list with the add IWAD menu.
 */
void MainView::openAddIWAD()
{
	clearLayout(iwadLayout);
	initAddIWAD();
}

/* Replaces the module list with the add module menu.
 */
void MainView::openAddModule()
{
	clearLayout(moduleLayout);
	initAddModule();
}

/* Replaces the IWAD list with the edit IWAD menu if an IWAD is selected. Its fields are filled
 * with info about the currently selected IWAD.
 */
void MainView::openEditIWAD()
{
	//Get selection
	QModelIndexList selectionList = iwadList->selectionModel()->selectedIndexes();
	if (selectionList.isEmpty() == false)
	{
		//Get selected IWAD name
		std::string name = selectionList[0].data().toString().toStdString();

		//Open menu if selected name is valid
		if (name != "")
		{
			clearLayout(iwadLayout);
			initEditIWAD(name);
		}
	}
	else
	{
		infoTextLabel->setText("An IWAD must be selected before it can be edited!");
	}
}

/* Replaces the module list with the edit module menu if a module is selected. Its fields are
 * filled with info about the currently selected module.
 */
void MainView::openEditModule()
{
	//Get selection
	QModelIndexList selectionList = moduleList->selectionModel()->selectedIndexes();
	if (selectionList.isEmpty() == false)
	{
		//Get selected module name
		std::string name = selectionList[0].data().toString().toStdString();

		//Open menu if selected name is valid and is not a category
		if (name != "" && selectionList[0].parent().row() != -1)
		{
			clearLayout(moduleLayout);
			initEditModule(name);
		}
		else
		{
			infoTextLabel->setText("Entire categories cannot be edited!");
		}
	}
	else
	{
		infoTextLabel->setText("A module must be selected before it can be edited!");
	}
}

//=================================================================================================
// Item Adders
//=================================================================================================

/* Adds an IWAD to the IWAD vector and list using information from the fields of the add IWAD menu.
 */
bool MainView::addIWAD()
{
	std::string name = editIWADNameField->text().toStdString();
	std::string filepath = editIWADFilepathField->text().toStdString();

	if (!name.empty() && !filepath.empty())
	{
		//Check if desired name is unique
		for (int i = 0; i < iwads->size(); i++)
		{
			if (name == iwads->at(i).getName())
			{
				infoTextLabel->setText("Cannot add a new IWAD with an existing name!");
				return false;
			}
		}

		//Add to IWAD vector if unique
		IWAD newIwad(name, filepath);
		iwads->push_back(newIwad);
		updateIWADModel();

		//Close menu
		openIWADList();

		return true;
	}
	else
	{
		infoTextLabel->setText("Cannot add an IWAD with no name or filepath!");
		return false;
	}
}

/* Adds an module to the module vector and list using information from the fields of the add
 * module menu.
 */
bool MainView::addModule()
{
	//Retrieve module name and category from form
	std::string name = editModuleNameField->text().toStdString();
	std::string category = editModuleCategoryField->text().toStdString();

	if (!name.empty() && !category.empty())
	{
		//Check if desired name is unique
		for (int i = 0; i < modules->size(); i++)
		{
			if (name == modules->at(i).getName())
			{
				infoTextLabel->setText("Cannot add a new module with an existing name!");
				return false;
			}
		}

		//Continue if there are files in the list
		if (fileModel->rowCount() > 0)
		{
			//Retrieve filepaths from list
			std::vector<std::string> files;
			for (int i = 0; i < fileModel->rowCount(); i++)
			{
				std::string test = fileModel->index(i, 0).data().toString().toStdString();
				files.push_back(test);
			}

			//Add module to vector
			Module newModule(name, category, files);
			modules->push_back(newModule);

			//Update and open module list
			updateModuleModel();
			openModuleList();

			return true;
		}
		else
		{
			infoTextLabel->setText("Cannot add a module with no files!");
			return false;
		}
	}
	else
	{
		infoTextLabel->setText("Cannot add a module with no name or category!");
		return false;
	}
}

//=================================================================================================
// Item Editors
//=================================================================================================

/* Changes the information of a selected IWAD to that of the fields of the edit IWAD menu.
 */
void MainView::editIWAD()
{
	if (editedIWAD != NULL)
	{
		std::string name = editIWADNameField->text().toStdString();
		std::string filepath = editIWADFilepathField->text().toStdString();

		if (!name.empty() && !filepath.empty())
		{
			//Change IWAD details to desired ones
			editedIWAD->setName(name);
			editedIWAD->setFilepath(filepath);

			//Update and open IWAD list
			updateIWADModel();
			openIWADList();
		}
		else
		{
			infoTextLabel->setText("Cannot remove an IWAD's name or filepath!");
		}
	}
}

/* Changes the information of a selected module to that of the fields of the edit module menu.
 */
void MainView::editModule()
{
	if (editedModule != NULL)
	{
		std::string name = editModuleNameField->text().toStdString();
		std::string category = editModuleCategoryField->text().toStdString();

		if (!name.empty() && !category.empty())
		{
			if (fileModel->rowCount())
			{
				//Change load list entry name
				QString oldName = editedModule->getName().c_str();
				QList<QStandardItem*> loadItems = loadModel->findItems(oldName);
				for (int i = 0; i < loadItems.size(); i++)
				{
					loadItems[i]->setText(QString(name.c_str()));
				}

				//Change module details to desired ones
				editedModule->setName(name);
				editedModule->setCategory(category);

				//Change filenames to desired ones
				std::vector<std::string> files;
				for (int i = 0; i < fileModel->rowCount(); i++)
				{
					files.push_back(fileModel->index(i, 0).data().toString().toStdString());
				}
				editedModule->setMods(files);

				//Update and open module list
				updateModuleModel();
				openModuleList();
			}
			else
			{
				infoTextLabel->setText("Cannot empty a module's file list!");
			}
		}
		else
		{
			infoTextLabel->setText("Cannot remove a module's name or category!");
		}
	}
}

//=================================================================================================
// Item Removers
//=================================================================================================

/* Removes the IWAD currently selected in the IWAD list from the list itself and the IWAD vector.
 */
void MainView::removeIWAD()
{
	//Get selection list
	QModelIndexList selectionList = iwadList->selectionModel()->selectedIndexes();

	//Continue if an IWAD has been selected
	if (selectionList.isEmpty() == false)
	{
		//Get selection
		QModelIndex selection = selectionList[0];
		std::string selectionName = selection.data().toString().toStdString();

		//Remove IWAD from IWAD list
		iwadModel->removeRow(selection.row());

		//Remove IWAD from IWAD vector
		for (int i = 0; i < iwads->size(); i++)
		{
			if (selectionName == iwads->at(i).getName())
			{
				iwads->erase(iwads->begin() + i);
			}
		}
	}
	else
	{
		infoTextLabel->setText("An IWAD must be selected before it can be removed!");
	}
}

/* Removes the module currently selected in the module list from the list itself and the
 * module vector.
 */
void MainView::removeModule()
{
	//Get selection list
	QModelIndexList selectionList = moduleList->selectionModel()->selectedIndexes();

	//Continue if a module has been selected
	if (selectionList.isEmpty() == false)
	{
		//Get selection
		QModelIndex selection = selectionList[0];
		QString selectionName = selection.data().toString();

		//Continue if not a category
		if (selection.parent().row() != -1)
		{
			//Remove module from module list
			moduleModel->removeRow(selection.row(), selection.parent());

			//Remove module from module vector
			for (int i = 0; i < modules->size(); i++)
			{
				if (selectionName.toStdString() == modules->at(i).getName())
				{
					modules->erase(modules->begin() + i);
				}
			}

			//Remove module from load list
			QList<QStandardItem*> loadItems = loadModel->findItems(selectionName);
			for (int i = 0; i < loadItems.size(); i++)
			{
				QModelIndex loadItemIndex = loadModel->indexFromItem(loadItems[i]);
				loadModel->removeRow(loadItemIndex.row());
			}

			//Remove category if no children remain after removal
			if (moduleModel->hasChildren(selection.parent()) == false)
			{
				moduleModel->removeRow(selection.parent().row());
			}
		}
		else
		{
			infoTextLabel->setText("Cannot remove an entire category!");
		}
	}
	else
	{
		infoTextLabel->setText("A module must be selected before it can be removed!");
	}
}

/* Removes the module currently selected in the load list from the list itself.
 */
void MainView::removeLoad()
{
	//Get selection list
	QModelIndexList selectionList = loadList->selectionModel()->selectedIndexes();

	//Continue if a module has been selected
	if (selectionList.isEmpty() == false)
	{
		//Get selection
		QModelIndex selection = selectionList[0];

		//Remove module from loadlist
		loadModel->removeRow(selection.row());
	}
	else
	{
		infoTextLabel->setText("A loaded module must be selected before it can be removed!");
	}
}

/* Removes the file currently selected in the file list of the add/edit module menu from the
 * list itself.
 */
void MainView::removeFileFromList()
{
	//Get selection list
	QModelIndexList selectionList = editModuleFileList->selectionModel()->selectedIndexes();

	//Continue if a module has been selected
	if (selectionList.isEmpty() == false)
	{
		//Get selection
		QModelIndex selection = selectionList[0];

		//Remove module from loadlist
		fileModel->removeRow(selection.row());
	}
	else
	{
		infoTextLabel->setText("A file must be selected before it can be removed!");
	}
}

//=================================================================================================
// Item Browsers
//=================================================================================================

/* Opens a file browser/dialog which allows the user to select a IWAD of their choice.
 * Upon selection, the path of the selected file replaces any text currently in the filepath field
 * of the add/edit IWAD menu.
 */
void MainView::browseIWAD()
{
	//Initialise file dialog
	QFileDialog dialog;
	dialog.setFileMode(QFileDialog::ExistingFile);
	dialog.setViewMode(QFileDialog::Detail);
	dialog.setNameFilter("*.wad *.pk3 *.zip *.iwad *.ipk3");

	//Open dialog and retrieve selected filenames
	QStringList filenames;
	if (dialog.exec())
		filenames = dialog.selectedFiles();

	//Change filepath to selected file
	if (filenames.isEmpty() == false)
		editIWADFilepathField->setText(filenames[0]);
}

/* Opens a file browser/dialog which allows the user to select files to append to the module.
 * Upon selection, the path of the selected files are added into the filelist of the add/edit
 * module menu.
 *
 * TODO: Consider renaming it to something else as it browses for files rather a module.
 */
void MainView::browseModule()
{
	//Initialise file dialog
	QFileDialog dialog;
	dialog.setFileMode(QFileDialog::ExistingFiles);
	dialog.setViewMode(QFileDialog::Detail);

	//Open dialog and retrieve selected filenames
	QStringList filenames;
	if (dialog.exec())
		filenames = dialog.selectedFiles();

	//Add files to list
	for (int i = 0; i < filenames.size(); i++)
	{
		//Check if filename is unique
		bool unique = true;
		for (int j = 0; j < fileModel->rowCount(); j++)
		{
			if (fileModel->findItems(filenames[i]).isEmpty() == false)
			{
				unique = false;
				break;
			}
		}

		//Add to file model if unique
		if (unique)
		{
			QStandardItem * item = new QStandardItem(filenames[i]);
			item->setDropEnabled(false);
			fileModel->appendRow(item);
		}
	}
}

/* Opens a file browser/dialog which allows the user to select an engine of their choice.
 * Upon selection, the path of the selected file replaces any text currently in the engine field.
 */
void MainView::browseEngine()
{
	//Initialise file dialog
	QFileDialog dialog;
	dialog.setFileMode(QFileDialog::ExistingFile);
	dialog.setViewMode(QFileDialog::Detail);

	//Open dialog and retrieve selected filenames
	QStringList filenames;
	if (dialog.exec())
		filenames = dialog.selectedFiles();

	//Change filepath to selected file
	if (filenames.isEmpty() == false)
		enginePathField->setText(filenames[0]);
}

//=================================================================================================
// Load List Manipulators
//=================================================================================================

/* Moves the module currently selected in the load list up one position if not already on top of
 * the list.
 */
void MainView::moveLoadUp()
{
	//Get selection list
	QModelIndexList selectionList = loadList->selectionModel()->selectedIndexes();

	if (selectionList.isEmpty() == false)
	{
		//Get selection
		QModelIndex selection = selectionList[0];
		QString selectionName = selection.data().toString();

		//Continue if selection is not at the top of the list
		if (selection.row() > 0)
		{
			//Get entry below selection
			QModelIndex aboveSelection = loadModel->index(selection.row()-1, 0);
			QString aboveSelectionName = aboveSelection.data().toString();

			//Swap data
			loadModel->setData(selection, aboveSelectionName);
			loadModel->setData(aboveSelection, selectionName);

			//Keep original selection after data swap
			loadList->selectionModel()->setCurrentIndex(aboveSelection,
					QItemSelectionModel::ClearAndSelect);
		}
	}
}

/* Moves the module currently selected in the load list down one position if not already bottom of
 * the list.
 */
void MainView::moveLoadDown()
{
	//Get selection list
	QModelIndexList selectionList = loadList->selectionModel()->selectedIndexes();

	if (selectionList.isEmpty() == false)
	{
		//Get selection
		QModelIndex selection = selectionList[0];
		QString selectionName = selection.data().toString();

		//Continue if selection is not at the bottom of the list
		if (selection.row() < loadModel->rowCount()-1)
		{
			//Get entry below selection
			QModelIndex belowSelection = loadModel->index(selection.row()+1, 0);
			QString belowSelectionName = belowSelection.data().toString();

			//Swap data
			loadModel->setData(selection, belowSelectionName);
			loadModel->setData(belowSelection, selectionName);

			//Keep original selection after data swap
			loadList->selectionModel()->setCurrentIndex(belowSelection,
					QItemSelectionModel::ClearAndSelect);
		}
	}
}

//=================================================================================================
// Getters
//=================================================================================================

/* Returns a pointer to the main layout
 */
QGridLayout * MainView::getLayout()
{
	return layout;
}

/* Returns a pointer to a desired IWAD
 */
IWAD * MainView::getIWAD(std::string name)
{
	for (int i = 0; i < iwads->size(); i++)
	{
		if (iwads->at(i).getName() == name)
			return &iwads->at(i);
	}

	return NULL;
}

/* Returns a pointer to a desired module
 */
Module * MainView::getModule(std::string name)
{
	for (int i = 0; i < modules->size(); i++)
	{
		if (modules->at(i).getName() == name)
			return &modules->at(i);
	}

	return NULL;
}

std::string MainView::getEnginePath()
{
	return enginePathField->text().toStdString();
}
