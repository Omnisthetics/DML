/*  This file (loadlist.cxx) is part of Doom Module Launcher - A Doom engine frontend.
 *
 *  Copyright (C) Omnisthetics
 *
 *  Doom Module Launcher is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Doom Module Launcher is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Doom Module Launcher.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "./loadlist.h"
#include <iostream>

LoadList::LoadList() : QListView()
{
	loadModel = NULL;
}

LoadList::LoadList(QStandardItemModel * newLoadModel) : QListView()
{
	loadModel = newLoadModel;
}

void LoadList::operator=(const LoadList & other)
{
	loadModel = other.loadModel;
}

void LoadList::mouseDoubleClickEvent(QMouseEvent * event)
{
	if (event->button() == Qt::LeftButton)
	{
		//Get index of selected module
		QModelIndex selection = indexAt(event->pos());
		loadModel->removeRow(selection.row());
	}
}
