/*  This file (mainview.h) is part of Doom Module Launcher - A Doom engine frontend.
 *
 *  Copyright (C) Omnisthetics
 *
 *  Doom Module Launcher is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Doom Module Launcher is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Doom Module Launcher.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MAINVIEW_H_
#define MAINVIEW_H_

#include "./iwad.h"
#include "./module.h"
#include "./modulelist.h"
#include "./loadlist.h"
#include <QIcon>
#include <QLabel>
#include <QObject>
#include <QListView>
#include <QLineEdit>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QFormLayout>
#include <QStackedLayout>
#include <QStandardItemModel>
#include <QSortFilterProxyModel>

class MainView : public QObject
{
	Q_OBJECT

private:
	//Data vectors
	std::vector<IWAD> * iwads;
	std::vector<Module> * modules;

	//Button icons
	QIcon plusIcon;
	QIcon minusIcon;
	QIcon dotsIcon;
	QIcon playIcon;
	QIcon crossIcon;
	QIcon tickIcon;
	QIcon folderIcon;
	QIcon upIcon;
	QIcon downIcon;
	QIcon infoIcon;

	//Main view menu layout members
	QVBoxLayout * iwadLayout;
	QVBoxLayout * moduleLayout;
	QVBoxLayout * loadLayout;
	QHBoxLayout * infoLayout;
	QGridLayout * layout;
	QPushButton * playButton;
	QLabel * infoIconLabel;
	QLabel * infoTextLabel;
	QLabel * enginePathLabel;
	QLineEdit * enginePathField;
	QPushButton * engineBrowseButton;

	//IWAD list members
	QSortFilterProxyModel * iwadModel;
	QHBoxLayout * iwadButtonLayout;
	QLabel * iwadLabel;
	QListView * iwadList;
	QPushButton * addIWADButton;
	QPushButton * editIWADButton;
	QPushButton * removeIWADButton;

	//Add/edit IWAD menu members
	QVBoxLayout * editIWADLayout;
	QFormLayout * editIWADFormLayout;
	QHBoxLayout * editIWADButtonLayout;
	QLabel * editIWADMenuLabel;
	QLabel * editIWADNameLabel;
	QLabel * editIWADFilepathLabel;
	QLineEdit * editIWADNameField;
	QLineEdit * editIWADFilepathField;
	QPushButton * editIWADBrowseButton;
	QPushButton * editIWADConfirmButton;
	QPushButton * editIWADCancelButton;
	IWAD * editedIWAD;

	//Module list members
	QSortFilterProxyModel * moduleModel;
	QHBoxLayout * moduleButtonLayout;
	QLabel * moduleLabel;
	ModuleList * moduleList;
	QPushButton * addModuleButton;
	QPushButton * editModuleButton;
	QPushButton * removeModuleButton;

	//Add/edit Module menu members
	QStandardItemModel * fileModel;
	QVBoxLayout * editModuleLayout;
	QFormLayout * editModuleFormLayout;
	QHBoxLayout * editModuleButtonLayout;
	QLabel * editModuleMenuLabel;
	QLabel * editModuleNameLabel;
	QLabel * editModuleCategoryLabel;
	QLineEdit * editModuleNameField;
	QLineEdit * editModuleCategoryField;
	QListView * editModuleFileList;
	QPushButton * editModuleBrowseButton;
	QPushButton * editModuleRemoveButton;
	QPushButton * editModuleConfirmButton;
	QPushButton * editModuleCancelButton;
	Module * editedModule;

	//Load list members
	QStandardItemModel * loadModel;
	QHBoxLayout * loadButtonLayout;
	QLabel * loadLabel;
	LoadList * loadList;
	QPushButton * moveLoadUpButton;
	QPushButton * moveLoadDownButton;
	QPushButton * removeLoadButton;
public:
	//Constructors and destructor
	MainView();
	MainView(std::vector<IWAD> * newIwads, std::vector<Module> * newModules, std::string newPath);
	~MainView();

	//Menu initialisers
	void initIWADList();
	void initAddIWAD();
	void initEditIWAD(std::string name);
	void initModuleList();
	void initAddModule();
	void initEditModule(std::string name);
	void initLoadList();

	//Misc.
	void execute();
	void updateIWADModel();
	void updateModuleModel();
	void clearLayout(QLayout * layout);

	//Menu openers
	void openIWADList();
	void openModuleList();
	void openAddIWAD();
	void openAddModule();
	void openEditIWAD();
	void openEditModule();

	//Item adders
	bool addIWAD();
	bool addModule();

	//Item editors
	void editIWAD();
	void editModule();

	//Item removers
	void removeIWAD();
	void removeModule();
	void removeLoad();
	void removeFileFromList();

	//Item browsers
	void browseIWAD();
	void browseModule();
	void browseEngine();

	//Load list manipulators
	void moveLoadUp();
	void moveLoadDown();

	//Getters
	QGridLayout * getLayout();
	IWAD * getIWAD(std::string name);
	Module * getModule(std::string name);
	std::string getEnginePath();
};

#endif
