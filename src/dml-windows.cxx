/*  This file (dml-windows.cxx) is part of Doom Module Launcher - A Doom engine frontend.
 *
 *  Copyright (C) Omnisthetics
 *
 *  Doom Module Launcher is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Doom Module Launcher is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Doom Module Launcher.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cstdlib>
#include <QWidget>
#include <QApplication>
#include <sys/stat.h>
#include "./iwad.h"
#include "./module.h"
#include "./modulelist.h"
#include "./loadlist.h"
#include "./mainview.h"

void load();
void save(MainView * view);
void loadIWADS();
void loadModules();
void loadEngine();
void saveIWADS();
void saveModules();
void saveEngine(std::string path);

const std::string IWAD_PATH = "./iwads.txt";
const std::string MODULE_PATH = "./modules.txt";
const std::string ENGINE_PATH = "./engine.txt";

std::vector<IWAD> iwads;
std::vector<Module> modules;
std::string enginePath;

int main(int argc, char * argv[])
{
	//Load IWADs and modules from saved files
	load();

	//Initialise Qt
	QApplication app(argc, argv);
	QWidget window;

	//Initialise mainview
	MainView mainView(&iwads, &modules, enginePath);

	//Initialise window
	window.setLayout(mainView.getLayout());
	window.setWindowTitle("Doom Module Launcher");
	window.show();

	//Execute Qt application
	int result = app.exec();

	//Save iwad and module list to respective files
	save(&mainView);

	return result;
}

void load()
{
	loadIWADS();
	loadModules();
	loadEngine();
}

void save(MainView * view)
{
	saveIWADS();
	saveModules();
	saveEngine(view->getEnginePath());
}

void loadIWADS()
{
	std::ifstream input(IWAD_PATH);

	while (input.good())
	{
		std::string name;
		std::string filepath;

		//Get name and filepath
		std::getline(input, name);
		std::getline(input, filepath);
		input.get();

		if (name != "" && filepath != "")
		{
			//Check if IWAD name is in use
			bool unique = true;
			for (int i = 0; i < iwads.size(); i++)
			{
				if (name == iwads[i].getName())
				{
					unique = false;
					break;
				}
			}

			//Add to IWAD vector if unique
			if (unique)
			{
				IWAD iwad(name, filepath);
				iwads.push_back(iwad);
			}
		}
	}

	input.close();
}

void loadModules()
{
	std::ifstream input(MODULE_PATH);

	while (input.good())
	{
		std::string name;
		std::string category;
		std::vector<std::string> mods;

		//Get name and category
		std::getline(input, name);
		std::getline(input, category);

		if (name != "" && category != "")
		{
			//Check if module name in use
			bool unique = true;
			for (int i = 0; i < modules.size(); i++)
			{
				if (name == modules[i].getName())
				{
					unique = false;
					break;
				}
			}

			//Get mod filenames
			std::string filename;
			std::getline(input, filename);
			while (filename != "")
			{
				mods.push_back(filename);
				std::getline(input, filename);
			}

			//Add to module vector if unique
			if (unique)
			{
				Module module = Module(name, category, mods);
				modules.push_back(module);
			}
		}
	}

	input.close();
}

void loadEngine()
{
	std::ifstream input(ENGINE_PATH);

	if (input.good())
	{
		getline(input, enginePath);
	}

	input.close();
}

void saveIWADS()
{
	std::ofstream output(IWAD_PATH, std::ofstream::trunc);

	//Write module details to file
	for (int i = 0; i < iwads.size(); i++)
	{
		//Write name and category
		output << iwads[i].getName() << std::endl;
		output << iwads[i].getFilepath() << std::endl;

		//Prevents an empty line at EOF
		if (i != iwads.size()-1)
			output << std::endl;
	}

	output.close();
}

void saveModules()
{
	std::ofstream output(MODULE_PATH, std::ofstream::trunc);

	//Write module details to file
	for (int i = 0; i < modules.size(); i++)
	{
		//Write name and category
		output << modules[i].getName() << std::endl;
		output << modules[i].getCategory() << std::endl;

		//Write module filenames
		std::vector<std::string> mods = modules[i].getMods();
		for (int j = 0; j < mods.size(); j++)
		{
			output << mods[j] << std::endl;
		}

		//Prevents an empty line at EOF
		if (i != modules.size()-1)
			output << std::endl;
	}

	output.close();
}

void saveEngine(std::string path)
{
	std::ofstream output(ENGINE_PATH, std::ofstream::trunc);

	output << path;

	output.close();
}
