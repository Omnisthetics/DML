/*  This file (module.cxx) is part of Doom Module Launcher - A Doom engine frontend.
 *
 *  Copyright (C) Omnisthetics
 *
 *  Doom Module Launcher is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Doom Module Launcher is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Doom Module Launcher.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "./module.h"
#include <iostream>
#include <cstdlib>

Module::Module()
{
	name = "";
	category = "";
	mods.clear();
}

Module::Module(std::string newName, std::string newCategory, std::vector<std::string> newMods)
{
	name = newName;
	category = newCategory;
	mods = newMods;
}

void Module::print()
{
	std::cout << "Name: " << name << std::endl;
	std::cout << "Category: " << category << std::endl;

	for (int i = 0; i < mods.size(); i++)
	{
		std::cout << mods[i] << std::endl;
	}

	std::cout << std::endl;
}

void Module::setName(std::string newName)
{
	name = newName;
}

void Module::setCategory(std::string newCategory)
{
	category = newCategory;
}

void Module::setMods(std::vector<std::string> newMods)
{
	mods = newMods;
}

std::string Module::getName()
{
	return name;
}

std::string Module::getCategory()
{
	return category;
}

std::vector<std::string> Module::getMods()
{
	return mods;
}
