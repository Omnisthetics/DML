/*  This file (module.h) is part of Doom Module Launcher - A Doom engine frontend.
 *
 *  Copyright (C) Omnisthetics
 *
 *  Doom Module Launcher is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Doom Module Launcher is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Doom Module Launcher.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MODULE_H_
#define MODULE_H_

#include <string>
#include <vector>

class Module
{
private:
	std::string name;
	std::string category;
	std::vector<std::string> mods;
public:
	Module();
	Module(std::string newName, std::string newCategory, std::vector<std::string> newMods);
	void print();
	void setName(std::string newName);
	void setCategory(std::string newCategory);
	void setMods(std::vector<std::string> newMods);
	std::string getName();
	std::string getCategory();
	std::vector<std::string> getMods();
};

#endif
