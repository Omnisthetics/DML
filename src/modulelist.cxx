/*  This file (modulelist.cxx) is part of Doom Module Launcher - A Doom engine frontend.
 *
 *  Copyright (C) Omnisthetics
 *
 *  Doom Module Launcher is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Doom Module Launcher is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Doom Module Launcher.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "./modulelist.h"
#include <iostream>

ModuleList::ModuleList() : QTreeView()
{
	loadModel = NULL;
}

ModuleList::ModuleList(QStandardItemModel * newLoadModel) : QTreeView()
{
	loadModel = newLoadModel;
}

void ModuleList::operator=(const ModuleList & other)
{
	loadModel = other.loadModel;
}

void ModuleList::mouseDoubleClickEvent(QMouseEvent * event)
{
	if (event->button() == Qt::LeftButton)
	{
		QModelIndex selection = indexAt(event->pos());

		//Continue if selection is not a category
		if (selection.parent().row() != -1)
		{
			//Get module name and category through list entries
			QString name = selection.data().toString();

			//Add module to load list if not in already
			if (loadModel->findItems(name).isEmpty())
			{
				QStandardItem * item = new QStandardItem(name);
				item->setDropEnabled(false);
				loadModel->invisibleRootItem()->appendRow(item);
			}
		}
	}
}
