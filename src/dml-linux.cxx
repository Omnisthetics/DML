/*  This file (dml-linux.cxx) is part of Doom Module Launcher - A Doom engine frontend.
 *
 *  Copyright (C) Omnisthetics
 *
 *  Doom Module Launcher is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Doom Module Launcher is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Doom Module Launcher.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cstdlib>
#include <QWidget>
#include <QApplication>
#include <sys/stat.h>
#include "./iwad.h"
#include "./module.h"
#include "./modulelist.h"
#include "./loadlist.h"
#include "./mainview.h"

void load();
void save(MainView * view);
void loadIWADS();
void loadModules();
void loadEngine();
void saveIWADS();
void saveModules();
void saveEngine(std::string path);
bool checkConfigFolder();

const std::string CONFIG_DIR = std::string(getenv("HOME")) + "/.config/dml";
const std::string IWAD_PATH = CONFIG_DIR + "/iwads.txt";
const std::string MODULE_PATH = CONFIG_DIR + "/modules.txt";
const std::string ENGINE_PATH = CONFIG_DIR + "/engine.txt";

std::vector<IWAD> iwads;
std::vector<Module> modules;
std::string enginePath;

int main(int argc, char * argv[])
{
	//Load IWADs and modules from saved files
	load();

	//Initialise Qt
	QApplication app(argc, argv);
	QWidget window;

	//Initialise mainview
	MainView mainView(&iwads, &modules, enginePath);

	//Initialise window
	window.setLayout(mainView.getLayout());
	window.setWindowTitle("Doom Module Launcher");
	window.show();

	//Execute Qt application
	int result = app.exec();

	//Save iwad and module list to respective files
	save(&mainView);

	return result;
}

/* Checks whether the default config folder is available. If so, calls the load functions below.
 */
void load()
{
	if (checkConfigFolder())
	{
		loadIWADS();
		loadModules();
		loadEngine();
	}
	else
	{
		std::cerr << "Failed to load library! Config folder could not be accessed!" << std::endl;
	}
}

/* Checks whether the default config folder is available. If so, IWADs, modules and the engine path
 * are saved into their respective files.
 */
void save(MainView * view)
{
	if (checkConfigFolder())
	{
		saveIWADS();
		saveModules();
		saveEngine(view->getEnginePath());
	}
	else
	{
		std::cerr << "Failed to save library! Config folder could not be accessed!" << std::endl;
	}
}

/* Reads the contents of the IWAD text file and appends said contents to the IWAD vector.
 */
void loadIWADS()
{
	std::ifstream input(IWAD_PATH);

	while (input.good())
	{
		std::string name;
		std::string filepath;

		//Get name and filepath
		std::getline(input, name);
		std::getline(input, filepath);
		input.get();

		if (name != "" && filepath != "")
		{
			//Check if IWAD name is in use
			bool unique = true;
			for (int i = 0; i < iwads.size(); i++)
			{
				if (name == iwads[i].getName())
				{
					unique = false;
					break;
				}
			}

			//Add to IWAD vector if unique
			if (unique)
			{
				IWAD iwad(name, filepath);
				iwads.push_back(iwad);
			}
		}
	}

	input.close();
}

/* Reads the contents of the module text file and appends said contents to the module vector.
 */
void loadModules()
{
	std::ifstream input(MODULE_PATH);

	while (input.good())
	{
		std::string name;
		std::string category;
		std::vector<std::string> mods;

		//Get name and category
		std::getline(input, name);
		std::getline(input, category);

		if (name != "" && category != "")
		{
			//Check if module name in use
			bool unique = true;
			for (int i = 0; i < modules.size(); i++)
			{
				if (name == modules[i].getName())
				{
					unique = false;
					break;
				}
			}

			//Get mod filenames
			std::string filename;
			std::getline(input, filename);
			while (filename != "")
			{
				mods.push_back(filename);
				std::getline(input, filename);
			}

			//Add to module vector if unique
			if (unique)
			{
				Module module = Module(name, category, mods);
				modules.push_back(module);
			}
		}
	}

	input.close();
}

/* Reads the single line stored in the engine path text file and saves it to a string variable.
 */
void loadEngine()
{
	std::ifstream input(ENGINE_PATH);

	if (input.good())
	{
		getline(input, enginePath);
	}

	input.close();
}

/* Saves the contents of the IWAD vector to a text file.
 */
void saveIWADS()
{
	std::ofstream output(IWAD_PATH, std::ofstream::trunc);

	//Write module details to file
	for (int i = 0; i < iwads.size(); i++)
	{
		//Write name and category
		output << iwads[i].getName() << std::endl;
		output << iwads[i].getFilepath() << std::endl;

		//Prevents an empty line at EOF
		if (i != iwads.size()-1)
			output << std::endl;
	}

	output.close();
}

/* Saves the contents of the module vector to a separate text file.
 */
void saveModules()
{
	std::ofstream output(MODULE_PATH, std::ofstream::trunc);

	//Write module details to file
	for (int i = 0; i < modules.size(); i++)
	{
		//Write name and category
		output << modules[i].getName() << std::endl;
		output << modules[i].getCategory() << std::endl;

		//Write module filenames
		std::vector<std::string> mods = modules[i].getMods();
		for (int j = 0; j < mods.size(); j++)
		{
			output << mods[j] << std::endl;
		}

		//Prevents an empty line at EOF
		if (i != modules.size()-1)
			output << std::endl;
	}

	output.close();
}

/* Saves the engine path and parameters stored in the main view object to a single line text file.
 */
void saveEngine(std::string path)
{
	std::ofstream output(ENGINE_PATH, std::ofstream::trunc);

	output << path;

	output.close();
}

/* Uses system functions to determine if the default config directory exists. If not, it attempts
 * to create the directory.
 */
bool checkConfigFolder()
{
	//Check if config folder exists
	struct stat sb;
	if (stat(CONFIG_DIR.c_str(), &sb) == -1)
	{
		std::cout << "Directory does not exist! Creating config directory..." << std::endl;

		//Create config folder
		if (mkdir(CONFIG_DIR.c_str(), 0755) == -1)
		{
			std::cout << "Failed to create config directory " << CONFIG_DIR << std::endl;
			return false;
		}
	}

	return true;
}
