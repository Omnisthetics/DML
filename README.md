# Doom Module Launcher
A C++/Qt Doom frontend which acts as both a simple library and launcher.
Users can create and add 'modules', which can comprise of multiple PWADs and files, to your library.
Saved modules can be added into a reorderable load list allowing users to mix and match various mods together.

## Supported Source Ports
* ZDoom/GZDoom
* PrBoom
* Chocolate Doom
* Eternity Engine
* Doomsday
* Doom Legacy
* Any source port that has a -iwad and -file command line argument

## Compilation
### Linux
```
qmake dml-linux.pro
make
```

### Windows
```
qmake dml-windows.pro
mingw32-make
windeployqt ./release/dml.exe
```

## Screenshots
![dml-screen1](/uploads/ab19f32a2465e0103b0097dd31b713c0/dml-screen1.png)
![dml-screen2](/uploads/16942e73455f159834cb1fbd0f647388/dml-screen2.png)
